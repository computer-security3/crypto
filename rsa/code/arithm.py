def gcd_rec(a,b):
    """returns gcd(a,b) with a and b integers, computed recursively"""
    if b == 0:
        return a
    else:
        return gcd_rec(b, a % b)

def gcd(a,b):
    """returns gcd(a,b) with a and b integers, computed iteratively"""
    if a == 0 :
        return b
    else:
        while b != 0 :
            a, b = b, a % b
        return a
    
def prime_factorisation(n):
    """
    Computes the prime factorisation of n
    
    Returns it as two list, one of prime factors, the other one as their exponents
    
    For instance, prime_factorisation(3888) returns [2,3], [4,5] since 3888 = 2^4 * 3^5
    """
    
    factors = []
    exponents = []
    last_factor = 1
    factor = 2
    
    #first, divide by two as much as you can
    while n % factor == 0 :
        if factor != last_factor : #first time we see the factor
            factors.append(factor)
            exponents.append(1)
            last_factor = factor
        else:
            exponents[-1]+=1
        n = n / factor
        
    #we are now sure n if odd: there are no more 2-factors
    last_factor = 2
    factor = 3
    while n != 1 :
        while n % factor == 0 :
            if factor != last_factor :
                factors.append(factor)
                exponents.append(1)
                last_factor = factor
            else:
                exponents[-1]+=1
            n = n / factor
        last_factor = factor
        factor += 2
        
    return factors, exponents

def print_prime_factorisation(n, factors, exponents):
    """
    Prints the prime factorisation of n
    
    Assumes the prime factorisation has been precomputed and is passed as parameter,
    factors is the list of factors and exponents their exponents
    """
    s = str(n) + " = "    
    for i in range(0, len(factors)) :
        if exponents[i] == 1:
            s += str(factors[i])
        else:
            s += str(factors[i]) + "^" + str(exponents[i])
            
        if i != len(factors) - 1 :
            s += " * "
            
    print(s)
                
    
def coprime(a,b):
    """"returns true if a and b are coprime, that is, if gcd(a,b) = 1"""
    return gcd(a,b) == 1
    
def bezout(a,b):
    """Computes the coefficients of Bézout's identity
    
    Concretely, given a and b integers, returns gcd(a,b) 
    and s and t such that gcd(a,b) = sa + tb, as an array of length 3
    """
    swap = a < b
    if swap:
        a, b = b, a
        
    old_s, s = 1, 0
    old_t, t = 0, 1    
    while b != 0:
        q = a // b
        a, b = b, a % b
        old_s, s = s, old_s - q * s
        old_t, t = t, old_t - q * t
        
    if not swap:
        return a, old_s, old_t
    else:
        return a, old_t, old_s
        
def print_bezout(a,b,bez):
    """Prints the linear combination sa + tb = gcd(a,b)
    
    This assumes that parameter bez is the precomputed 
    Bézout's coefficients of the identity sa + tb = gcd(a,b)
    """
    print("gcd(", a, ",", b, ") is ", bez[0], " and ", bez[0], " = ", bez[1], " * ", a, " + ", bez[2], " * ", b )    
    
def mult_inv(a, m):
    """
    Returns the multiplicative inverse of a (modulo m), assuming a < m
    If gcd(a,m) != 1, the inverse doesn't exist and an ArithmeticError is raised
    """
    bez = bezout(a, m)
    if bez[0] != 1:
        raise ArithmeticError("Element ", a, " has no multiplicative inverse modulo ", m, " since gcd(", a, ",", m, ") != 1.")
    
    return bez[1] % m
    
def mod_exp(a, b, m):
    """
    Computes (a^b) mod m 
    """
    r = 1
    while b > 0:        
        if b % 2 != 0:
            r = (r * a) % m
        a = (a * a) % m
        b = b // 2
    return r
    
def check_crt(a1, n1, a2, n2):
    """
    Checks if the hypothesises of the chinese remainder theorem hold, that is, given
      x = a1 (mod n1),
      x = a2 (mod n2),
    checks that gcd(n1, n2) = 1, and a1 < n1 and a2 < n2
    """
    return a1 < n1 and a2 < n2 and gcd(n1, n2) == 1

def crt(a1, n1, a2, n2):
    """
    Find a solution to the system of congruences
      x = a1 (mod n1)
      x = a2 (mod n2)
    where it is assumed gcd(n1, n2) = 1, and a1 < n1 and a2 < n2
    """
    bez = bezout(n1, n2)
    return (a1 * bez[2] * n2 + a2 * bez[1] * n1) % (n1 * n2)
    
def euler(fact, exp):
    """
    Given the factorisation of some integer, computes Euler's Phi function
    
    The factorisation as to be provided as two arrays of the same size, 
    the first one with the factors and the second one with the exponents
    """
    res = 1
    for i in range(len(fact)):
        res *= (fact[i]**(exp[i]-1)) * (fact[i] - 1)
    return res