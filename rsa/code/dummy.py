import arithm

print("gcd(84,30) is ", arithm.gcd_rec(84,30))
print("gcd(84,30) is ", arithm.gcd(84,30))

bez = arithm.bezout(973, 301)
arithm.print_bezout(973, 301, bez) #ok

bez = arithm.bezout(240, 46)
arithm.print_bezout(240, 46, bez) #ok

bez = arithm.bezout(3,4)
arithm.print_bezout(3, 4, bez) #ok
print(arithm.crt(0, 3, 3, 4))

bez = arithm.bezout(9, 11)
arithm.print_bezout(9, 11, bez) #ok
print(arithm.check_crt(6, 9, 4, 11))
print("A solution to x = 6 (mod 9) and x = 4 (mod 11) is ", arithm.crt(6, 9, 4, 11))

inv = arithm.mult_inv(15, 26)
print("Mult. inverse of 15 (mod 26) is ", inv)
print((15 * inv) % 26)