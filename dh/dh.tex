\documentclass[a4paper,11pt]{article}

%\usepackage[showframe]{geometry} %use this if you want to check margins
\usepackage{geometry}
\geometry{centering,total={160mm,250mm},includeheadfoot}

%auxiliairy files
\input{header.tex} %usepackage and configurations
\input{cmds.tex} %user defined commands
\input{crypto.tex}

%title
\newcommand{\doctitle}{Introduction to cryptography}
\newcommand{\docsubtitle}{The Diffie-Hellman key exchange from scratch}
\newcommand{\tdyear}{2023 - 2024}
\author{R. Absil}

%language input
\lstset{language=c++,
		morekeywords={constexpr,nullptr}}

%language={[x86masm]Assembler}
%language=c++, morekeywords={constexpr,nullptr}
%language=Java

\begin{document}
\pagestyle{fancy}

\maketitle

Blah

\tableofcontents

\section{Introduction}

While RSA is a well-known and widely used public key algorithm, the first published algorithm of this kind was the Diffie-Hellman key exchange (DHKE), published in 1976~\cite{dh} by Whitfield Diffie and Martin Hellman, strongly influenced by the work of Ralph Merkle.

In practice, this algorithm allows to ``exchange'' a secret key by communicating over an insecure channel. Note that, with the RSA algorithm, it's not hard to exchange a secret key over an insecure channel:
\begin{enumerate}
\item one of the actors generates the key on his side,
\item ciphers it with the public key of the other actor,
\item sends that ciphered key,
\item and the other actor deciphers the key with his own private key,
\end{enumerate}
as illustrated on Figure~\ref{fig:rsa-key-exch}. Note that this scheme is in no way related to the RSA algorithm, as any public key cipher will work, but is named that way because it was proposed in the original paper of the publication of the RSA algorithm~\cite{rsa}.

\begin{figure}[!htp]
\begin{center}
\scalebox{0.75}{
\begin{tikzpicture}
\node[draw, inner sep = 10pt,fill=yellow!50, text width=3cm] (gen) at (-5,3) {Symmetric key generation};
\draw[->,>=stealth] (gen) -- (-1.2,3);

\draw[fill=blue!30] (-1.2,2.25) rectangle (1.8,3.75);
\begin{scope}[shift={(0,3)},scale=.25]
\labelledkey{Dandelion}{Session key}
\end{scope}

\node[draw, inner sep = 20pt,fill=yellow!50] (cipher) at (5,3) {\tt Cipher};
\draw[->,>=stealth] (1.8,3) -- (cipher);

\node[draw, fill=cyan!80, label={right:\small Ciphered key}, text width=2.3cm, align=center] (ciphered) at (5.1,0) {\tt 6B 9C 64 AF\\23 F1 C4 D2\\3E 65 12 1A};

\draw[->,>=stealth] (cipher) -- (5,0.8);

\begin{scope}[shift={(9,3)},scale=.25]
\labelledkey{Green}{Public key of Bob}
\end{scope}

\begin{scope}[shift={(9,-5)},scale=.25]
\labelledkey{red!80}{Private key of Bob}
\end{scope}

\draw[very thick] (-5,-2) -- (11,-2);

\node[draw, inner sep = 20pt,fill=yellow!50] (uncipher) at (5,-5) {\tt Uncipher};
\draw[->,>=stealth,very thick, dotted] (5,-0.8) -- (uncipher) node[midway, above, sloped, xshift=10pt] {Send};

\draw[fill=blue!30] (-1.2,-4.25) rectangle (1.8,-5.75);
\begin{scope}[shift={(0,-5)},scale=.25]
\labelledkey{Dandelion}{Session key}
\end{scope}

\draw[->,>=stealth] (8,3) -- (cipher);
\draw[->,>=stealth] (8,-5) -- (uncipher);
\draw[->,>=stealth] (uncipher) -- (1.8,-5);

\node at (-4,-0.5) {\LARGE Alice};
\node at (-4,-3.5) {\LARGE Bob};
\end{tikzpicture}}
\caption{The RSA key exchange}
\label{fig:rsa-key-exch}
\end{center}
\end{figure}

While simple, this key-exchange scheme suffers from a drawback: if at some point the private key of an actor is compromised, every single symmetric key ever exchanged with the help of that private key is also compromised. If a lot of symmetric keys were exchanged that way, this cannot be ignored, as a great deal of confidentiality might me lost. At most, we would like that if a private key is compromised, that it only compromises a single symmetric key. This quality feature that the RSA key exchange lacks is called \emph{forward secrecy}.

The Diffie-Hellman key exchange has the same objective as the RSA key exchange: exchange a secret key over an insecure channel. However, it achieves forward secrecy, and therefore works differently:
\begin{enumerate}
\item both actors agree on using some public piece of information,
\item both actors each generate a temporary pair of keys,
\item actors exchange their public keys,
\item each actor ``combines'' his own private key with the public key he received: this process crafts a secret common to both actors,
\item the common secret his used to derive a symmetric key,
\end{enumerate}
as illustrated on Figure~\ref{fig:dh-key-echange}. This schemes clearly achieves forward secrecy, as the keypairs that are used are temporary and trashed as soon as the symmetric key has been generated. This way, if a keypair ever gets compromised, only the associated symmetric key is compromised.

\begin{figure}[!htp]
\begin{center}
\scalebox{.5}{
\begin{tikzpicture}
\node at (-4,1.5) {\LARGE Alice};
\node at (-4,-1.5) {\LARGE Bob};
\draw[very thick] (-5,0) -- (0.5,0);

\begin{scope}[shift={(-5,3)}, scale=.25]
\labelledkey{Green}{Public session key of Alice}
\end{scope}

\begin{scope}[shift={(-5,-3)}, scale=.25]
\labelledkey{Green}{Public session key of Bob}
\end{scope}

\node[draw, fill=yellow!50, minimum height=1cm, minimum width=3cm] (ca) at (3,3) {Combine};
\node[draw, fill=yellow!50, minimum height=1cm, minimum width=3cm] (cb) at (3,-3) {Combine};

\draw[->, >=stealth, thick, dotted] (-3,3) to[out=0,in=180] node[pos=.2, above, sloped] {\textbf{Send}} (cb.west);
\draw[->, >=stealth, thick, dotted] (-3,-3) to[out=0,in=180] node[pos=.2, below, sloped] {\textbf{Send}} (ca.west);

\begin{scope}[shift={(3,5)}, scale=.25]
\labelledkey{red!80}{Private session key of Alice}
\end{scope}

\begin{scope}[shift={(3,-5.5)}, scale=.25]
\labelledkey{red!80}{Private session key of Bob}
\end{scope}

\node[draw, fill=blue!40, minimum height=1cm, text width=4cm, align=center] (ci) at (3,0) {Public common piece of information};
\draw[->,>=stealth, thick] (ci) -- (ca);
\draw[->,>=stealth, thick] (ci) -- (cb);

\draw[->,>=stealth, thick] (3,4.5) -- (ca);
\draw[->,>=stealth, thick] (3,-4.5) -- (cb);

\node[draw, fill=Dandelion, minimum height=1cm, text width=3cm, align=center] (cs) at (9,0) {Common secret};
\draw[->,>=stealth, thick, dotted] (ca) to[out=0,in=90] (cs.north);
\draw[->,>=stealth, thick, dotted] (cb) to[out=0,in=270] (cs.south);

\node[draw, fill=yellow!50, minimum height=1cm, text width=4cm, align=center] (kdf) at (15,0) {Key derivation function};
\draw[->,>=stealth, thick] (cs) -- (kdf);

\draw[->,>=stealth, thick] (kdf) -- (19,0);

\begin{scope}[shift={(20,0)}, scale=.25]
\labelledkey{Dandelion}{Secret session key}
\end{scope}
\end{tikzpicture}}
\caption{The Diffie-Hellman key exchange}
\label{fig:dh-key-echange}
\end{center}
\end{figure}

As we can see, it is important that the ``combine'' algorithm is design in such a way that it has the same output when executed with
\begin{itemize}
\item the public key of Bob and the private key of Alice,
\item the private key of Bob and the public key of Alice.
\end{itemize}
Furthermore, an attacker who has the common piece of information Alice and Bob agreed to work with alongside both of their public keys cannot craft the secret used to derive the symmetric key.

The algorithm relies fact that exponentiation in $\IZ^\cdot_p$ (with $p$ prime), is
\begin{enumerate}
\item a one-way function,
\item commutative,
\end{enumerate}
that is, we have
\begin{equation}
k = \left(\alpha^x\right)^y \equiv_p \left(\alpha^y\right)^x. \label{eqref:dhke-intro}
\end{equation}
The value $k \equiv_p \left(\alpha^x\right)^y \equiv_p \left(\alpha^y\right)^x$ is the common secret that is going to be used to derive a symmetric key between the two parties.

While it is clear that $\cdot$ is commutative, and consequently that Equation~\ref{eqref:dhke-intro} holds, there is some work to do in order to prove that exponentiation is one-way. Indeed, we need to show that inverting this operator requires an unfeasible amount of time, in the same way that inverting RSA without the private key is unfeasible.

\newcommand{\pubk}{k_{pub}}
\newcommand{\prk}{k_{pr}}

More formally, the DHKE between two actors $A$ and $B$ works as follows:
\begin{description}
\item[Setup] \hfill
    \begin{itemize}
    \item choose a large prime $p$,
    \item choose $\alpha \in \Set{2, 3, \dots, p-2}$,
    \item publish $\alpha$ and $p$;
    \end{itemize}
\item[Keypair generation and exchange] \hfill
    \begin{itemize}
    \item $A$ picks his private key $\prk(A)$ at random and computes his public key $\pubk(A)$ as $\pubk(A) \equiv_p \alpha^{\prk(A)}$,
    \item $B$ proceeds similarly and gets $\prk(B)$ and $\pubk(B)$,
    \item both actors exchange their public keys;
    \end{itemize}
\item[Secret generation and key derivation] \hfill
    \begin{itemize}
    \item $A$ computes $s \equiv_p \pubk(B)^{\prk(A)}$,
    \item $B$ proceeds similarly, and computes $s \equiv_p \pubk(A)^{\prk(B)}$,
    \item both actors use $s$ to derivate a symmetric key.
    \end{itemize}
\end{description}
Note that $s$ is guaranteed to be the same for $A$ and $B$, by Equation~\ref{eqref:dhke-intro}. Indeed, on the side of $A$, we have
\begin{equation*}
s \equiv_p \pubk(B)^{\prk(A)} = \left(\alpha^{\prk(B)}\right)^{\prk(A)} \equiv_p \alpha^{\prk(A) \cdot \prk(B)},
\end{equation*}
and on the side of $B$, we have
\begin{equation*}
s \equiv_p \pubk(A)^{\prk(B)} = \left(\alpha^{\prk(A)}\right)^{\prk(B)} \equiv_p \alpha^{\prk(A) \cdot \prk(B)},
\end{equation*}
and that is all there is to the proof of the correctness of the Diffie-Hellman key exchange: it is remarkably simple.

However, we cannot choose $\alpha$ at random: it needs to follow some properties that are detailed in Section~\ref{sec:dhke}, otherwise exponentiation is not going to be a one-way function.

\begin{example}
At setup, Alice and Bob agree to use $p = 23$ and $\alpha = 5$, where $\alpha$ has the aforementioned properties. Then,
    \begin{enumerate}
    \item Alice chooses $\prk(A) = 4$, and computes $\pubk(A) = \pubk(A) \equiv_p \alpha^{\prk(A)}$, that is,
        \begin{align*}
        \pubk(A) &= 5^4 \mod 23 \\
                 &= 4
        \end{align*}
    \item Bob chooses $\prk(A) = 3$, and computes $\pubk(B) = \pubk(B) \equiv_p \alpha^{\prk(B)}$, that is,
        \begin{align*}
        \pubk(B) &= 5^3 \mod 23 \\
                 &= 10
        \end{align*}
    \item Alice and Bob exchange their public keys $\pubk(A)$ and $\pubk(B)$,
    \item Alice computes
        \begin{align*}
        s &= \pubk(B)^{\prk(A)} \mod p\\
          &= 10^4 \mod 23\\
          &= 18
        \end{align*}
    \item Bob computes
        \begin{align*}
        s &= \pubk(A)^{\prk(B)} \mod p\\
          &= 4^3 \mod 23\\
          &= 18
        \end{align*}
    \item Alice and Bob trash their keypairs,
    \item Alice and Bob use $s$ (guaranteed to be the same), to generate a symmetric key that are going to use for their current session.
    \end{enumerate}
\end{example}
Note that, in the above example, we have $\pubk(A) = \prk(A)$, but it's usually not the case.

\section{A bit of group theory}\label{sec:groups}

\newcommand{\group}[1]{\langle #1 \rangle}

The Diffie-Hellman key exchange works in \emph{abelian groups}, an algebraic structure simpler than the rings used for RSA. This section details the necessary theoretical elements in order to prove that exponentiation in carefully chosen groups is one-way.

\begin{defbox}{}{group}
An \emph{abelian group} is a set of elements $G$ together with a binary operator $\circ$ with the following properties:
\begin{enumerate}
\item $\circ$ is closed: for all $a, b \in G$, we have $a \circ b \in G$,
\item $\circ$ is associative: for all $a, b, c \in G$, we have $(a \circ b) \circ c = a \circ (b \circ c)$,
\item $\circ$ is commutative: for all $a,b \in G$, we have $a \circ b = b \circ a$,
\item there is a neutral element $e \in G$ such that for all $a \in G$ we have $a \circ e = e \circ a = e$,
\item $\circ$ is invertible: for all $a \in G$, there exist an \emph{inverse} $a^{-1}$ such that $a \circ a^{-1} = a^{-1} \circ a = e$.
\end{enumerate}
Such a groups is noted $\group{G, \circ}$.
\end{defbox}

In cryptography, we use both additive groups where $\circ$ is noted $+$, and multiplicative groups where $\circ$ is noted $\cdot$.

\begin{example} We notice that
\begin{itemize}
\item $\group{\IZ, +}$ is an abelian group: it is clearly associative and commutative, the neutral is $0$ and the inverse of $a$ is $-a$.
\item $\group{\IZ\setminus\{0\}, \cdot}$ is not an abelian group: while it is associative, commutative and the neutral is $1$, no element $a$ has an inverse, with the exceptions of $1$ and $-1$.
\end{itemize}
\end{example}

\newcommand{\gzn}{\IZ^*_n}
\newcommand{\gzp}{\IZ^*_p}
\newcommand{\gznn}[1]{\IZ^*_{#1}}

\begin{defbox}{}{}
Let $n \in \IN$, we write $\gzn$ the set of naturals coprime with $n$, together with operation~$\cdot$, the multiplication modulo $n$.
\end{defbox}

This set $\gzn$ is going to be at the core of the Diffie-Hellman key exchange, which is why we're interested in studying its properties. In the same vein, we're going to enumerate further properties that this set has, such as the fact that it is an abelian group, \emph{finite} and \emph{cyclic}.

\begin{propbox}{}{zn-abelian}
$\gzn$ is an abelian group.
\end{propbox}

\begin{proof}
Beyond inversion and closeness of $\cdot$, all properties of an abelian group are clearly verified.

To show that $\cdot$ is closed, we need to show that $ab$ is coprime with $n$ for all $a, b$ coprime with $n$. If $a$ and $b$ are coprime with $n$, there share no common divisor with $n$. Consequently, neither does their product.

By property~\ref{prop:modinv}, inverses exist because every element is coprime with $n$. We can build these inverses either with the Extended Euclidean Algorithm, or with Fermat's little Theorem.
\end{proof}

\begin{example}
Let $n = 9$, consider $\gznn{9}$ where we have $G = \Set{1, 2, 4, 5, 7, 8}$. We can build the multiplication table of $\cdot$ as illustrated on Table~\ref{tab:mult}. Table~\ref{tab:inv} illustrates the inverses of each element, computed with the Extended Euclidean Algorithm.

\begin{table}[!htp]
\begin{center}
\hfill
\begin{subtable}{.4\textwidth}
\begin{tabular}{c|cccccc}
$\cdot$ & 1 & 2 & 4 & 5 & 7 & 8\\
\hline
1 & 1 & 2 & 4 & 5 & 7 & 8\\
2 & 2 & 4 & 8 & 1 & 5 & 7\\
4 & 4 & 8 & 7 & 2 & 1 & 5\\
5 & 5 & 1 & 2 & 7 & 8 & 4\\
7 & 7 & 5 & 1 & 8 & 4 & 2\\
8 & 8 & 7 & 5 & 4 & 2 & 1
\end{tabular}
\caption{Multiplication modulo $9$}
\label{tab:mult}
\end{subtable}
\hfill
\begin{subtable}{.4\textwidth}
\begin{tabular}{c|cccccc}
$a$ & 1 & 2 & 4 & 5 & 7 & 8\\
\hline
\rule{0pt}{15pt}
$a^{-1}$ & 1 & 5 & 7 & 2 & 4 & 8
\end{tabular}
\caption{Multiplicative inverses modulo $9$}
\label{tab:inv}
\end{subtable}
\hfill
\caption{Structure of $\gznn{9}$}
\end{center}
\end{table}
\end{example}

\newcommand{\euler}[1]{\varphi(#1)}
\newcommand{\zm}{\IZ_m}

\begin{defbox}{}{}
A \emph{finite} abelian group $\group{G, \circ}$ is an abelian group with a finite number of elements. We denote the \emph{cardinality} of $G$ by $|G|$
\end{defbox}

\begin{example}
The following abelian groups are finite:
\begin{itemize}
\item $\group{\zm, \cdot}$: the cardinality of $\zm$ is $|\zm| = m$, since $\zm = \Set{0, 1, \dots, n-1}$;
\item $\gzn$: since $\gzn$ is defined as the set of naturals coprime with $n$, we have $|\gzn| = \euler{n}$. For instance, $\gznn{9}$ has $\euler{9} = 3^2 - 3^1 = 6$ elements.
\end{itemize}
\end{example}

\begin{nott}
Let $\group{G, \circ}$, we write
\begin{equation*}
a^n = \left\{
    \begin{array}{ll}
    e & \text{if } n = 0,\\
    \underbrace{a \circ a \circ \cdots \circ a}_{n~\textrm{times}} & \text{if } n > 0,\\[1.5em]
    \underbrace{a^{-1} \circ a^{-1} \circ \cdots \circ a^{-1}}_{n~\textrm{times}} & \text{if } n < 0.
    \end{array}
\right.
\end{equation*}
\end{nott}

With the above notation, we can check that the usual power rules work in the case of groups, that is,
\begin{align*}
a^m \circ a^n = a^{m+n},\\
\left(a^m\right)^n = \left(a^n\right)^m,\\
a^m = a^n \ssi a^{m-n} = e.
\end{align*}

\begin{defbox}{}{}
Let $\group{G, \circ}$ a finite group and $a \in G$, $\group{a}$ denotes the set
\begin{equation*}
\Set{a^{-m} \circ a^n \suchthat m,n \in \IN}
\end{equation*}
together with operation $\circ$.
\end{defbox}

Basically, $\group{a}$ is the set of powers of $a$ and $a^{-1}$. Later, we will show that $\gzn$ can be built as some $\group{a}$.

\begin{propbox}{}{generated}
Let $\group{G, \circ}$ be an abelian group and $a \in G$, $\group{a}$ is an abelian group. We say that $\group{a}$ is a \emph{subgroup} of $G$.
\end{propbox}

\begin{proof}
We first notice that $\group{a}$ is not empty, since it contains $a$, $a^{-1}$ and consequently also $e$. Furthermore,
\begin{enumerate}
\item $\circ$ is both associative and commutative, directly following the definition of $\circ$ in $G$,
\item $\circ$ is closed: if $a^m, a^n \in \group{a}$, then $a^m \circ a^n = a^{m+n} \in \group{a}$,
\item $\circ$ is invertible: if $a^n \in \group{a}$, then $\left(a^n\right)^{-1} = \left(a^{-1}\right)^n \in \group{a}$.
\end{enumerate}
\end{proof}

We're now going to take a particular interest in these powers, as it is going to be the basis for the robustness of the Diffie-Hellman key exchange and similarly related problems.

\begin{defbox}{}{}
Let $\group{G, \circ}$ a finite abelian group and $a \in G$, the \emph{order} of $a$, written $\ord(a)$, is the smallest integer $k$ such that
\begin{equation*}
a^k = e
\end{equation*}
\end{defbox}

So far, it isn't clear that $k$ actually exists, as powering $a$ could never lead to $e$. It is however the case, as described by the following property.

\begin{propbox}{}{order}
Let $\group{G, \circ}$ be a finite group with $a \in G$,
\begin{enumerate}
\item $\ord(a)$ is finite,
\item $\group{a}$ is a finite abelian group,
\item $\ord(a) = |\group{a}|$.
\end{enumerate}
\end{propbox}

\begin{proof}
Let $\group{G, \circ}$ be a finite group with $a \in G$.
\begin{enumerate}
\item The set of powers of $a$ is contained in $G$. Considering $G$ is finite, the set of powers of $a$ is consequently also finite, by the pigeonhole principle. This means that there exist two integers $0 < m < n$ such that $a^m = a^n$, that is
\begin{align*}
     & a^m = a^n \\
\ssi~& \left(a^n\right)^{-1} a^m = 1 \\
\ssi~& a^{-n} a^m = 1 \\
\ssi~& a^{m-n} = 1 \\
\end{align*}
and $a^{m-n}$ is itself some power of $a$. Setting $k$ as $m-n$ proves that $k$ is finite, and hence also $\ord(a)$.
\item By Property~\ref{prop:generated}, we know that $\group{a}$ is an abelian group. Furthermore, since the set of powers of $a$ is finite, $\group{a}$ is a finite group.
\item Since $\ord(a) = k$ is finite, we can enumerate powers of $a$ as
\begin{equation*}
a, a^2, a^3, \dots, a^{k-1}, a^k = e
\end{equation*}
and all these powers are distinct, since $k$ is minimum\footnote{Otherwise, we can ``simplify'' powers as in the first point of this proof to get a lower $k$.}.

Furthermore, the sequence $a, a^2, a^3, \dots, a^{k-1}$ is repeated\footnote{This observation is \emph{very} important.} for higher powers of $a$, since $a^k = 1$. For instance, we have $a^{k+2} = a^k a^2 = e~a^2 = a^2$. This means there are \emph{only} these $k$ elements in $\group{a}$, and hence $\ord(a) = |\group{a}|$.
\end{enumerate}
\end{proof}

\begin{example}
Let us consider $\gznn{11}$, and $a = 3$. We can compute the order of $3$ by powering it iteratively:
\begin{alignat*}{2}
a^1 &= 3\\
a^2 &= a \cdot a   &&= 9\\
a^3 &= a^2 \cdot a &&= 27 \equiv_{11} 5\\
a^4 &= a^3 \cdot a &&= 15 \equiv_{11} 4\\
a^5 &= a^4 \cdot a &&= 12 \equiv_{11} 1
\end{alignat*}
From the last line, it follows that $\ord(3) = 5$.

Furthermore, if we keep powering $a$, we can see that the sequence $3, 9, 5, 4, 1$ repeats itself, for instance,\linebreak
\begin{minipage}{.45\textwidth}
\begin{alignat*}{2}
a^6 &= a^5 \cdot a &&=3\\
a^7 &= a^6 \cdot a &&= 9\\
a^8 &= a^7 \cdot a &&= 27 \equiv_{11} 5\\
a^9 &= a^8 \cdot a &&= 15 \equiv_{11} 4\\
a^{10} &= a^9 \cdot a &&= 12 \equiv_{11} 1
\end{alignat*}
\end{minipage}
\begin{minipage}{.45\textwidth}
\begin{alignat*}{2}
a^{11} &= a^{10} \cdot a &&= 3\\
a^{12} &= a^{11} \cdot a   &&= 9\\
a^{13} &= a^{12} \cdot a &&= 27 \equiv_{11} 5\\
a^{14} &= a^{13} \cdot a &&= 15 \equiv_{11} 4\\
a^{15} &= a^{14} \cdot a &&= 12 \equiv_{11} 1
\end{alignat*}
\end{minipage}\linebreak
and so on. With these computations, we can also see that
\begin{equation*}
\group{a} = \Set{1, 3, 4, 5, 9}
\end{equation*}
and that $|\group{a}| = 5 = \ord(3)$. Furthermore, $5$ divides $10 = |\gznn{11}|$.
\end{example}

In the above example, we see that in any finite groups, after ``powering an element enough'', a sequence appears. There is a special case of groups, called \emph{cyclic groups}, where the repeated sequence is the complete group.

\begin{defbox}{}{}
A group $G$ containing an element $\alpha$ with maximum order
\begin{equation*}
\ord(\alpha) = |G|
\end{equation*}
is said to be \emph{cyclic}. Such an element $\alpha$ is called a \emph{generator} of $G$.
\end{defbox}

Such elements $\alpha$ are called generators because, when powered, they actually generate the entire group.

\begin{example}\label{ex:gz11}
Let us check that $a = 2$ is a generator of $\gznn{11}$. We have
\begin{equation*}
\gznn{11} = \Set{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}
\end{equation*}
and $|\gznn{11}| = 10$. We need to check that $\ord(2) = 10$. We have\\
\begin{minipage}{.45\textwidth}
\begin{align*}
a^1 &= 2\\
a^2 &= 4\\
a^3 &= 8\\
a^4 &= 16 \equiv_{11} 5\\
a^5 &= 32 \equiv_{11} 10
\end{align*}
\end{minipage}
\begin{minipage}{.45\textwidth}
\begin{align*}
a^6 &= 64 \equiv_{11} 9\\
a^7 &= 128 \equiv_{11} 7\\
a^8 &= 256 \equiv_{11} 3\\
a^9 &= 512 \equiv_{11} 6\\
a^{10} &= 1\,024 \equiv_{11} 1
\end{align*}
\end{minipage}\linebreak
meaning that we actually have $\ord(2) = 10 = |\gznn{11}|$, which implies that $\gznn{11}$ is cyclic. Indeed, powering $a$ generates the entire group, as illustrated on Figure~\ref{fig:cyclic}. On this figure, arrows denote multiplication by $2$.

\begin{figure}[!htp]
\begin{center}
\begin{tikzpicture}
\pgfmathsetmacro{\order}{10}
\pgfmathsetmacro{\generator}{2}

\foreach \i in {1, ..., \order}
{
    \pgfmathtruncatemacro{\angle}{(360 / \order) * (\i - 1)}
    \node (n\i) at (\angle:2.5) {$\i$};
}

\foreach \i in {1, ..., \order}
{
    \pgfmathtruncatemacro{\endnode}{mod((\i * \generator, \order + 1))}
    \draw[->, >=stealth, very thick, red!60] (n\i) -- (n\endnode);
}
\end{tikzpicture}
\caption{The structure of $\gznn{11}$, as generated by $2$.}
\label{fig:cyclic}
\end{center}
\end{figure}

On this figure, we see that the order in which elements are generated looks somewhat random. This seemingly arbitrary relationship between $i$ and $a^i$ is at the core of the robustness of the Diffie-Hellman key exchange.
\end{example}

From the above example, we can actually characterise all groups $\gzn$ that are cyclic.

\begin{thmbox}{}{cyclic}
Let $p$ be a prime, $\gzp$ is a finite cyclic abelian group.
\end{thmbox}

In order to prove this property at the core of the robustness of the Diffie-Hellman key exchange, we need a few intermediary results.

\begin{example}
Example~\ref{ex:gz11} already illustrated that $\gznn{11}$ was cyclic, since $11$ is prime. We also checked that $2$ is an element of order $10$.
\end{example}

\begin{thmbox}{Lagrange Theorem\footnotemark}{lagrange}
Let $G$ be an finite cyclic abelian group and $a \in G$,
\begin{itemize}
\item $|\group{a}|$ divides $|G|$
\item $\ord(a)$ divides $|G|$.
\end{itemize}
\end{thmbox}

\footnotetext{While we only prove this result for cyclic abelian groups and only their subgroups $\group{a}$ only, the theorem is actually much more powerful, as it holds for groups that are neither abelian nor cyclic, and for any of their subgroups. We don't prove this stronger result, since in the scope of this document we are only interested in $\gzp$.}

\begin{proof}
By Property~\ref{prop:order}, we have $|\group{a}| = \ord(a)$, so we only need to prove that $\ord(a)$ divides $|G|$.

First we show that for any $a \in G$, that $a^{|G|} = e$, a result we will later use. We note $aG = \Set{ag \suchthat g \in G}$. Since $a$ is invertible, $aG = G$. Multiplying the elements of each part together, we have
\begin{equation*}
a^{|G|} \cdot \prod_{g \in G} g = \prod_{g \in G} g,
\end{equation*}
meaning that $a^{|G|} = 1$ (since elements $g$ of $G$ are all invertible).

We now write $k = \ord(a)$, we will show that $k$  divides $|G|$. Consider the euclidean division of $|G|$ by $k$, that is, $|G| = qk + r$, for some quotient $q$ and remainder $r$ such that $0 \leqslant r < k$. Since $a^{|G|} = e$, we have
\begin{align*}
     & a^{qk + r} = e\\
\ssi~&a^{qk} \cdot a^r = e\\
\ssi~&\left(a^k\right)^q \cdot a^r = e\\
\ssi~&a^r = e & \text{since } a^k = e\\
\ssi~&r = 0 & \text{since } r < k
\end{align*}
meaning that $k$ divides $|G|$ because the remainder is zero.
\end{proof}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%proofs below ok, but useless

%\begin{lembox}{}{square}
%Let $p$ be a prime, if $a^2 \equiv_p 1$, then $a \equiv_p 1$ or $a \equiv_p p-1$.
%\end{lembox}
%
%\begin{proof}
%We perform Euclidean division of $a$ by $p$, and write $a = kp + r$, with $r < p$. We have then
%\begin{align*}
%     & a^2 \equiv_p 1\\
%\ssi~& (kp + r)^2 \equiv_p 1\\
%\ssi~& k^2p^2 + 2kpr + r^2 \equiv_p 1\\
%\ssi~& r^2 \equiv_p 1
%\end{align*}
%since both $k^2p^2$ and $2kpr$ are divisible by $p$.
%
%Since $r^2 \equiv_p 1$, it means $p \divs r^2 - 1$, that is, $p \divs (r - 1) (r + 1)$, meaning $p \divs r-1$, \emph{or} $p \divs r+1$.
%\begin{enumerate}
%\item If $p \divs r - 1$, since $r < p$, we have $r - 1 < p$. However, the only integer lower than $p$ that $p$ can divide is $0$. Hence, if $r - 1 = 0$, we have $r = 1$.
%\item If $p \divs r + 1$, since $r < p$, we have $0 < r + 1 \leqslant p$. However, the only strictly positive integer lower or equal to $p$ that $p$ can divide is $p$. Hence, if $r + 1 = p$, we have $r = p-1$.
%\end{enumerate}
%\end{proof}

%\begin{thmbox}{Wilson's Theorem}{wilson}
%Let $p$ be a prime, then $(p-1)! \equiv_p -1$.
%\end{thmbox}
%
%\begin{proof}
%We know that since $p$ is a prime, by Property~\ref{prop:modinv}, each $a \in \Set{1, 2, \dots, p-1}$ has a unique multiplicative inverse modulo $p$.
%
%In the factorial product, we will pair\footnote{This pairing of elements with their inverses is well defined because there is an even number of factors in $(p-1)!$.} each factor with its inverse, save for $1$ and $p-1$. This pairing is well-defined because
%\begin{itemize}
%\item there is an even number of factors in $(p-1)!$,
%\item if a factor $f$ is its own inverse $f^{-1}$, then $f^2 \equiv_p 1$, and by Lemma~\ref{lem:square}, it means $f \equiv_p 1$ or $f \equiv_p p - 1$.
%\end{itemize}
%
%Each pair will be cancelled to $1$, and the remaining product becomes
%\begin{align*}
%(p - 1)! & \equiv_p 1 \cdot (p-1) \\
%         & \equiv_p -1
%\end{align*}
%\end{proof}

%We are now ready to prove that $\gzp$ is cyclic.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%complete bullshit below

%\begin{proof}[Proof of Theorem~\ref{thm:cyclic}]
%By Property~\ref{prop:zn-abelian}, we already know that $\gzp$ is an abelian group. It is also clearly finite. We then only need to prove that it is cyclic, that is, that we can find a generator. Let us prove the existence of an element $\alpha$ of order $|\gzp| = p - 1$.
%
%For every $a \in \gzp$, we will build the set $S_a = \Set{a, 2a, 3a, \dots, (p-1)a}$. Since $a$ is coprime to $p$, each element in $S_a$ is distinct modulo $p$. Therefore, $|\gzp| = |S_a|$.
%
%Let us now consider the product (modulo $p$) of all elements in $S_a$. We have
%\begin{align*}
%         & a \cdot 2a \cdot 3a \cdot \dots \cdot (p-1) a\\
%\equiv_p~& (p-1)! \cdot a^{p-1} & \text{since } \cdot \text{ is both commutative and associative}\\
%\equiv_p~& (-1) \cdot a^{p-1} & \text{by Wilson's Theorem}
%\end{align*}
%which implies that $a^{p-1} \equiv_p -1$. Therefore, $a^{p-1}$ is an element of order $2$ in $\gzp$, since
%\begin{equation*}
%\left(a^{p-1}\right)^2 \equiv_p (-1) \cdot (-1) \equiv_p 1.
%\end{equation*}
%\end{proof}

%\begin{proof}[Proof of Theorem~\ref{thm:cyclic}]
%By Property~\ref{prop:zn-abelian}, we already know that $\gzp$ is an abelian group. It is also clearly finite. We then only need to prove that it is cyclic, that is, that we can find a generator. Let us prove the existence of an element $\alpha$ of order $|\gzp| = p - 1$.
%
%Let us consider the product $(p-1)!$ of every element of $\gzp$. We will pair each element of this product with its inverse, with the exception of $1$ and $p-1$. This pairing is well-defined because
%\begin{itemize}
%\item there is an even number of factors in $(p-1)!$,
%\item if a factor $f$ is its own inverse $f^{-1}$, then $f^2 \equiv_p 1$, and by Lemma~\ref{lem:square}, it means $f \equiv_p 1$ or $f \equiv_p p - 1$ (which we didn't pair).
%\end{itemize}
%
%Each pair will be cancelled to $1$, and the remaining product becomes
%\begin{align*}
%(p - 1)! &\equiv_p 1 \cdot (p-1)\\
%         &\equiv_p (p-1)
%\end{align*}
%\end{proof}



\section{The Diffie-Hellman key exchange}\label{sec:dhke}

\bibliographystyle{abbrv}
\bibliography{biblio}

\end{document}

